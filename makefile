all: references.pdf

references.bbl: references.tex refs.tex

refs.tex: refs.bib get-refs
	perl ./get-refs refs.bib > refs.tex

%.bbl: %.tex/Users/Habil/Dropbox/UOW/Habil_work/IMPORTANT/refs/refs.tex
	latex $*
	latex $*
	bibtex $*

%.dvi: %.bbl
	latex $*
	latex $*

%.pdf: %.dvi
	dvipdf $*.dvi

clean:
	rm -f *.aux *.log *.blg *.dvi *.bbl *.ps *.pdf refs.tex
