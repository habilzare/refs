#!/usr/bin/env bash

## add-external.sh: add an external link to refs to an SVN directory elsewhere in the noble repository

## $Revision: 4968 $
## Copyright 2011 Michael M. Hoffman <mmh1@uw.edu>

set -o nounset
set -o pipefail
set -o errexit

if [ $# -gt 1 ]; then
    echo usage: "$0" [DIRNAME]
    exit 2
fi

# default directory: current directory
DIRNAME=${1:-.}

# ^ the top-level directory in this repo
# http://svnbook.red-bean.com/en/1.5/svn.advanced.externals.html
# XXX: this doesn't check to see if it's already in the property
PROPERTY="$(svn propget svn:externals "$DIRNAME")"
if [ "$PROPERTY" ]; then
    PROPERTY="$PROPERTY"$'\n'
fi
PROPERTY="${PROPERTY}^/refs/trunk refs"

svn propset svn:externals "$PROPERTY" "$DIRNAME"

if [ "$DIRNAME" == "." ]; then
    COMMAND="svn update"
else
    COMMAND="svn update $DIRNAME"
fi

echo >&2 "Use '$COMMAND' to check out the refs."
echo >&2 'Use \bibliography{refs/refs} in your TeX source to import the refs.'
echo >&2 "Other users will only see the external mapping after you commit."
